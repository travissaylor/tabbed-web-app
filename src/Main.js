import React, { Component } from "react";
import {
  Route,
  NavLink,
  HashRouter
} from 'react-router-dom';

import Home from './Home';
import Stuff from './Stuff';
import Contact from './Contact';
import Kittens from './Kittens';

class Main extends Component {
  render() {
    return (
      <HashRouter>
        <div className="pageWrapper">
          <h1>Travis's Tabbed App</h1>
          <ul className="header">
            <li><NavLink to="/">Home</NavLink></li>
            <li><NavLink to="/stuff">Stuff</NavLink></li>
            <li><NavLink to="/contact">Contact</NavLink></li>
            <li><NavLink to="/kittens">Kittens</NavLink></li>
          </ul>
          <div className="content">
            <Route exact path="/" component={Home} />
            <Route path="/stuff" component={Stuff} />
            <Route path="/contact" component={Contact} />
            <Route path="/kittens" component={Kittens} />
          </div>
        </div>
      </HashRouter>
    );
  }
}

export default Main;
